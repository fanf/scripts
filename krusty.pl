#!/usr/bin/perl
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

use warnings;
use strict;

use FindBin;

if ("@ARGV" eq "-h" or "@ARGV" eq "--help") {
	print STDERR <<'USAGE';
usage:	git diff | krusty.pl - | patch -p0
	krusty.pl [files]

When invoked with '-', reformat any .c or .h file mentioned in the
input diff, using `uncrustify | clang-format`, and emit a patch
containing only the formatting changes that overlap with changed
lines in the input diff.

With a list of filenames, krusty.pl gets its input from `git diff
HEAD [files]` and pipes its output to patch. You might like to `ln
-s krusty.pl .git/hooks/pre-commit`

Exit status is zero if there were no changes.
USAGE
	exit 1;
}

my $git_dir = qx{git rev-parse --show-toplevel};
chomp $git_dir;
chdir $git_dir;
die "could not find .uncrustify.cfg in git toplevel directory\n"
    unless -f ".uncrustify.cfg";

$ENV{UNCRUSTIFY_CONFIG} = "$FindBin::RealBin/uncrustify.cfg";
die "could not find $ENV{UNCRUSTIFY_CONFIG}\n"
    unless -f $ENV{UNCRUSTIFY_CONFIG};

my $hunk_re = qr{^\@\@ \-(\d+),(\d+) \+(\d+),(\d+) \@\@};

# current file name and hunk ranges for each file
my $file;
my %hunk;

my $input = \*STDIN;
if ("@ARGV" ne "-") {
	open my $git, "git diff HEAD @ARGV |"
	    or die "open git diff HEAD @ARGV | failed\n";
	$input = $git;
}

for my $line (<$input>) {
	if (my ($old_first,$old_len,$new_first,$new_len) =
	    $line =~ m{$hunk_re}) {
		# The file on disk corresponds to the new version in the
		# input diff (as produced by `git diff`), so we save the
		# new line range for calculating hunk intersections.
		my $new_last = $new_first + $new_len - 1;
		my $range = [$new_first, $new_last];
		push $hunk{$file}->@*, $range if defined $file;
	} elsif ($line =~ m{^--- ([^\t\n]+)}) {
		# Strictly speaking we should keep track of whether we are
		# inside a hunk or not, because a --- line is ambiguous
		# without that context.
		$file = $1;
		undef $file unless $file =~ m{\.[ch]$};
	} else {
		# ignore other lines
	}
}

my $changed = 0;
for my $file (keys %hunk) {
	my @hunk = $hunk{$file}->@*;

	open my $diff,
	    "uncrustify -f $file 2>/dev/null | clang-format | diff -u $file - |"
	    or die "open uncrustify | clang-format | diff | failed for $file\n";

	my $keep = 1; # keep the preamble
	my $hunks = 0;
	my @patch;
	for my $line (<$diff>) {
		if (my ($old_first,$old_len,$new_first,$new_len) =
		    $line =~ m{$hunk_re}) {
			# The file on disk corresponds to the old version in
			# the pipeline diff, so we use the old line range for
			# calculating hunk intersections.
			my $old_last = $old_first + $old_len - 1;
			my $this = [$old_first, $old_last];
			# Does this hunk intersect any of the input hunks?
			undef $keep;
			for my $that (@hunk) {
				$keep ||= $this->[0] <= $that->[1]
				      &&  $that->[0] <= $this->[1];
			}
			$hunks += $keep;
			$changed |= $keep;
		}
		push @patch, $line if $keep;
	}

	if ($hunks) {
		if ("@ARGV" ne "-") {
			open my $patch, "| patch $file"
			    or die "open | patch failed for $file\n";
			$patch->print(@patch);
		} else {
			print @patch;
		}
	}
}

exit $changed;
