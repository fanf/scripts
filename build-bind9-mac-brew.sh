#!/opt/homebrew/bin/bash
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

set -eux

TOP=$(pwd | sed 's|Code/isc|Inst|')

scripts=$HOME/Code/isc/scripts

fast=true

time_configure() {

	(cd $scripts && make jlibtool)

	sphinxlib=/opt/homebrew/opt/sphinx-doc/libexec
	if [ ! -d $sphinxlib/lib/python3.10/site-packages/sphinx_rtd_theme ]
	then
		$sphinxlib/bin/pip install sphinx-rtd-theme
	fi

	export PKG_CONFIG_PATH=/opt/homebrew/opt/openssl@1.1/lib/pkgconfig
	sccache=/opt/homebrew/bin/sccache
	llvm=/opt/homebrew/opt/llvm@15
	lld=$llvm/bin/ld64.lld # --ld-path=$lld

	$fast && $scripts/fix-configure

	#CC="$sccache $llvm/bin/clang" \
	#CFLAGS="-gfull -O3 -fsanitize=address,undefined" \
	#LDFLAGS="-gfull -L$llvm/lib -Wl,-rpath,$llvm/lib" \
	#CFLAGS="-gfull -O3 -I$TOP/include" \
	#LDFLAGS="-gfull -L$TOP/lib -L$llvm/lib -Wl,-rpath,$llvm/lib" \
	CC=$scripts/cc \
	PYTHON=$HOME/Inst/python/bin/python \
	PYTEST=$HOME/Inst/python/bin/pytest \
	SPHINX_BUILD=/opt/homebrew/opt/sphinx-doc/bin/sphinx-build \
	OPENSSL_CFLAGS=$(pkg-config --cflags openssl) \
	OPENSSL_LIBS=$(pkg-config --libs openssl) \
	time dash configure \
		--config-cache \
		--disable-silent-rules \
		--enable-dependency-tracking \
		--enable-developer \
		--enable-dnstap \
		--enable-full-report \
		--enable-querytrace \
		--enable-singletrace \
		--without-gssapi \
		--with-libidn2 \
		--with-lmdb=/opt/homebrew \
		--prefix=$TOP

#		--enable-fuzzing=libfuzzer \
#		--without-jemalloc \
#		--enable-leak-detection \
#
}

time {
	for d in .ln bin include lib key run zone
	do
		mkdir -p $TOP/$d
	done
	rm -rf		$TOP/run
	rm -rf		$TOP/var
	ln -shf bin	$TOP/sbin
	ln -shf .ln	$TOP/var
	ln -shf .	$TOP/.ln/var
	ln -shf .	$TOP/.ln/run
	ln -shf ../run	$TOP/.ln/named

	if ! grep U_FORTIFY_SOURCE configure.ac ||
	   ! time_configure
	then
		fortify=_FORTIFY_SOURCE
		sed -i~ "s/\"-D$fortify/\"-U$fortify -D$fortify/" configure.ac

		rm -f ar-lib compile config.cache missing libtool ltmain.sh
		time autoreconf -fi

		time_configure
	fi

	# ensure we are not using slowdown scripts as expected
	if $fast
	then
		for x in ar-lib compile install-sh missing libtool ltmain.sh
		do
			rm -f $x
			ln -s $scripts/disabled $x
		done
	fi

	time make -j install
	time make -j -C tests check TESTS=""

	# because my config is different from the committed config
	git co doc/misc/options doc/man/named.conf.5in

} 2>&1 | tee build.log
