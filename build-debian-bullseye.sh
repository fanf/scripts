#!/bin/sh
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

set -eux

TOP=~/isc/usr

time {
	rm -f config.cache
	time autoreconf -fi

	CFLAGS="-fsanitize=undefined,address -g -Os" \
	LDFLAGS="-fsanitize=undefined,address -g" \
	time ./configure \
		--config-cache \
		--disable-silent-rules \
		--enable-dependency-tracking \
		--enable-developer \
		--enable-dnstap \
		--enable-full-report \
		--enable-querytrace \
		--enable-singletrace \
		--without-gssapi \
		--without-libidn2 \
		--with-lmdb \
		--prefix=$TOP

	time make -j all

	mkdir -p	$TOP/.ln
	mkdir -p	$TOP/bin
	mkdir -p	$TOP/key
	mkdir -p	$TOP/run
	mkdir -p	$TOP/zone
	rm -rf		$TOP/sbin
	rm -rf		$TOP/var
	ln -shf bin	$TOP/sbin
	ln -shf .ln	$TOP/var
	ln -shf .	$TOP/.ln/var
	ln -shf .	$TOP/.ln/run
	ln -shf ../run	$TOP/.ln/named

	time make -j install

} 2>&1 | tee build.log
