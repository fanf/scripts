#!/bin/sh

# the MMAUTHTOKEN should be set in the crontab; its value comes from
# the web browser cookie of the same name

set -eux

# trim 0 so the numbers are not treated as octal
hours=$(date +%H)
hours=${hours#0}
minute=$(date +%M)
minute=${minute#0}

angle=$(( (hours * 360 + minute * 6) / 24 - 180 ))

echo angle $angle

img=$HOME/Pictures/TonyFinch
src=$img.jpeg
dst=$img-rotated.png

magick $src \
       -background transparent \
       -virtual-pixel background \
       -distort ScaleRotateTranslate "1.0 $angle" \
       -resize 320x320 \
       $dst

# translate the username (e.g. fanf) into a numerical user ID
uid=$(
	curl --verbose \
	     --header "Authorization: Bearer $MMAUTHTOKEN" \
	     https://mattermost.isc.org/api/v4/users/username/$USER |
	jq -r .id
)

curl --verbose \
     --header "Authorization: Bearer $MMAUTHTOKEN" \
     --form image=@"$dst" \
     https://mattermost.isc.org/api/v4/users/$uid/image
