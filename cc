#!/bin/sh

set -eu

case "$*" in
(-print-prog-name=ld)
	echo $HOME/Code/isc/scripts/ld
	;;
(*)
	exec /opt/homebrew/bin/sccache /opt/homebrew/opt/llvm/bin/clang \
	     --ld-path=/opt/homebrew/bin/ld64.mold "$@"
	;;
esac
