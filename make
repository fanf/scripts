#!/bin/sh

set -eux

{
	make -wj install
	make -wj -C tests check TESTS=""
	make -wj -C fuzz check TESTS=""
	make -wj -C bin/tests check TESTS=""
	git checkout doc/misc/options

} 2>&1 | ts -i %.S | ts -s %.S | tee -a build.log
