#!/bin/sh

case $# in
(1)	zone=$1
	file=$1
	;;
(2)	zone=$1
	file=$2
	;;
(*)	echo 1>&2 "usage: new-raw-zone.sh zone [file]"
	exit 1
	;;
esac

named-compilezone -F raw -o $file $zone - <<'ZONE'
$TTL 1h
@	SOA	localhost. root.localhost. 1 1h 1h 1w 1h
	NS	localhost.
ZONE
