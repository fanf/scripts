#!/usr/bin/perl

use warnings;
use strict;
use 5.16.0;

use File::Find;

my @file;

find sub {
	my $f = $File::Find::name;
	push @file, $f if $f =~ m{\.c$} and $f !~ m{/tests/};
}, qw(bin lib);

my %fundef;
my %countdef;
my %countloc;

for my $file (@file) {
	open my $fh, '<', $file or die "open < $file: $!\n";
	while (<$fh>) {
		$countloc{$file}++;
		next unless m{^([a-z0-9_]+)\(};
		$fundef{$1} = $file;
		$countdef{$file}++;
	}
}

my %funcall;
my %funref;

for my $file (@file) {
	open my $fh, '<', $file or die "open < $file: $!\n";
	while (<$fh>) {
		next unless m{[^A-Za-z0-9_]([a-z0-9_]+)([^A-Za-z0-9_])};
		next unless defined $fundef{$1};
		next if $fundef{$1} eq $file;
		if ($2 eq '(') {
			$funcall{$1} = $file;
		} else {
			$funref{$1} = $file;
		}
	}
}

my %edge;
for my $fun (keys %funcall) {
	$edge{"\"$funcall{$fun}\" -> \"$fundef{$fun}\""} = "";
}

my %cluster;
for my $file (@file) {
	$file =~ m{^(.*?)([^/]+)$};
	$cluster{$1} .= "\t\t\"$file\"\n";
}

print "digraph {\n";
for my $edge (sort keys %edge) {
	print "\t$edge\n";
}
for my $dir (sort keys %cluster) {
	print "\tsubgraph \"cluster $dir\" {\n";
	print $cluster{$dir};
	print "\t}\n";
}
print "}\n";
