#!/usr/bin/perl
#
# flatten anchors / references and `<<` map merges in a YAML file
#
# also ensures values in a `variables` map are strings (this is
# specifically for `.gitlab-ci.yml` files that need to be read by a
# golang yaml parser that cannot set environment variables to numbers)
#
# reads input from a filename or stdin, prints output to stdout

use warnings;
use strict;

use YAML::PP;
use Data::Dumper;

my $yaml = YAML::PP->new(
	boolean => 'JSON::PP',
	cyclic_refs => 'fatal',
        duplicate_keys => 0,
	footer => 0,
	header => 0,
	schema => [qw[ + Merge ]],
    );

sub flatten;
sub flatten {
	my $obj = shift;
	if (ref($obj) eq 'ARRAY') {
		my $i = 0;
		while ($i < scalar $obj->@*) {
			my $elem = $obj->[$i];
			if (ref($elem) eq 'ARRAY') {
				splice $obj->@*, $i, 1, $elem->@*;
			} else {
				$i++;
			}
		}
	} elsif (ref($obj) eq 'HASH') {
		for my $key (keys $obj->%*) {
			flatten $obj->{$key} if ref $obj->{$key};
		}
	}
}

sub stringify {
	my $obj = shift;
	if (ref($obj) eq 'HASH') {
		my $copy = {};
		for my $key (keys $obj->%*) {
			$copy->{$key} = "" . $obj->{$key};
		}
		return $copy;
	} else {
		return $obj;
	}
}

sub uniquify;
sub uniquify {
	my $obj = shift;
	if (ref($obj) eq 'ARRAY') {
		return [map { uniquify($_) } $obj->@*];
	} elsif (ref($obj) eq 'HASH') {
		my $copy = {};
		for my $key (keys $obj->%*) {
			if ($key eq 'variables') {
				$copy->{$key} = stringify($obj->{$key});
			} else {
				$copy->{$key} = uniquify($obj->{$key});
			}
		}
		return $copy;
	} else {
		return $obj;
	}
}

my $obj = $yaml->load_string(join '', <>);

flatten $obj;

print $yaml->dump_string(uniquify $obj);
