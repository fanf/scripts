#!/opt/homebrew/bin/bash
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

{
	set -eux

	TOP=$HOME/Inst/libuv

	sccache --start-server & </dev/null >/dev/null 2>&1
	sccache --zero-stats

	dash autogen.sh

	llvm=/opt/homebrew/opt/llvm@15

	CC="sccache $llvm/bin/clang" \
	CFLAGS="-gfull -O2 -fsanitize=thread,undefined" \
	LDFLAGS="-gfull -L$llvm/lib -Wl,-rpath,$llvm/lib" \
	dash configure \
		--config-cache \
		--disable-silent-rules \
		--prefix=$TOP

	make -j clean
	make -j install

	sccache -s

} 2>&1 | ts -i %.S | ts -s %.S | tee build.log
